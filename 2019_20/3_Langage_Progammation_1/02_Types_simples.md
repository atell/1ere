# Les types de base

## Les chaînes de caractère

Les chaînes de caractères sont de type  `str`. Un simple caractère est aussi une
chaîne de caractère ce qui n'est pas vrai dans d'autres langages.

Les chaînes de caractères sont représentées  par des caractères entre simples ou
doubles guillemets:

```python
>>> "Bonjour le Monde"         
 'Bonjour le Monde'

>>> 'Bonjour le Monde'         
 'Bonjour le Monde'

>>> "C'est la vie"             
 "C'est la vie"

>>> 'C'est la vie'             
  File "<ipython-input-28-11202c0f01b2>", line 1
    'C'est la vie'
         ^
SyntaxError: invalid syntax


>>> 'Il a dit "oui !"'         
 'Il a dit "oui !"'

>>> "Il a dit "oui !""         
  File "<ipython-input-30-abb082a6ed83>", line 1
    "Il a dit "oui !""
                 ^
SyntaxError: invalid syntax

>>> print('C\'est "Oui!"')     
C'est "Oui!"
```



On peut **concaténer** deux chaînes avec l'opérateur `+`:

```python
>>> 'Bonjour' + ' le Monde'    
 'Bonjour le Monde'

>>> '12' + '34'                
 '1234'

>>> '12' + ' + ' + '34'        
 '12 + 34'

>>> '12' + 34                  
------------------------------------
TypeErrorTraceback (most recent call last)
<ipython-input-36-afb1c3d28cba> in <module>
----> 1 '12' + 34

TypeError: can only concatenate str (not "int") to str
```



### Convertir en chaîne de caractères

On effectue  souvent des  échanges avec  le monde extérieur  via des  chaînes de
caractères (import/export  de fichiers) car  c'est aussi ce que  se transmettent
les commandes du `shell` depuis les années 1970. 

*Entre nous*

>Avec l'arrivée de `PowerShell`
>en 2002,  les choses  évoluent enfin...mais  c'est une  autre histoire  que nous
>raconterons plus tard.


On a donc  besoin de convertir des  objets en `str` (nous  verrons l'an prochain
cela plus en détail). Il suffit d'utiliser en `Python` la commande `str`:

```python
>>> str(12 + 5)                
 '17'

>>> str(12) + str(5)           
 '125'

>>> a = input('Donne-moi un entier : ')                              
Donne-moi un entier : 12

>>> a                                                                
 '12'

>>> type(a)                                                          
 str
```


## Les entiers

Les entiers de `Python`  n'ont pas de limitation en taille  si ce n'est l'espace
mémoire qu'ils occupent. 

Les opérateurs habituels sont `+, -, *, **, //, %`.

La fonction `int` a différents usages. Voyons la doc:

```python
>>> ?int                                                            
Init signature: int(self, /, *args, **kwargs)
Docstring:     
int([x]) -> integer
int(x, base=10) -> integer

Convert a number or string to an integer, or return 0 if no arguments
are given.  If x is a number, return x.__int__().  For floating point
numbers, this truncates towards zero.

If x is not a number or if base is given, then x must be a string,
bytes, or bytearray instance representing an integer literal in the
given base.  The literal can be preceded by '+' or '-' and be surrounded
by whitespace.  The base defaults to 10.  Valid bases are 0 and 2-36.
Base 0 means to interpret the base from the string as an integer literal.
>>> int('0b100', base=0)
4
```




Par exemple:

```python
>>> int('12') + int('5')                                            
 17

>>> a = int(input('Donne-moi un entier : '))                        
Donne-moi un entier : 12

>>> a + 5                                                           
 17

>>> 5 // 2                                                          
 2

>>> 5 % 2                                                           
 1

>>> int('bac', 16)                                                  
 2988

>>> int('1101', 2)                                                  
 13
```



## Les nombres à virgule flottante

Nous parlerons en détail de la représentation des nombres à virgule flottante en
machine plus  tard. Ils sont  représentés en `Python`  par le type  `float`. Les
opérateurs de base sont `+, -, *, **, /`

À noter la syntaxe pour utiliser les puissance de 10.  
Le nombre `$1,602\times 10^{-19}$` s'écrit `1.602e-19`.

Si l'on a besoin  de la racine carrée ou de toute  autre fonction élémentaire ou
même de `$\pi$`, on
peut les charger depuis la bibliothèque `math` :

```python
>>> import math
>>> math.sqrt(2)
1.4142135623730951
>>> math.pi
3.141592653589793
>>> math.sqrt(math.pi**2)
3.141592653589793
>>> math.sqrt(-1)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: math domain error
```

La  fonction `float`  va agir  un peu  comme `int`  pour convertir  un `str`  en
`float`:

```python
>>> x = float(input('Donne-moi un nombre à virgule flottante : '))
Donne-moi un nombre à virgule flottante : 3.1415926
>>> x
3.1415926
>>> x.is_integer()
False
>>> x / 2
1.5707963
```

Il  s'agit  de  conversions  *explicites*  car   on  précise  la  nature  de  la
conversion. (Définition  du dictionnaire  : *Qui est  énoncé complètement  et ne
peut prêter à aucune contestation*).


Certaines conversions  peuvent être  *implicites* (Définition du  dictionnaire :
*Qui, sans être énoncé formellement, découle naturellement de quelque chose*):

```python
>>> a = 3
>>> type(a)
<class 'int'>
>>> b = a * 2.0
>>> type(b)
<class 'float'>
>>> math.sqrt(4)
2.0
>>> type(math.sqrt(4))
<class 'float'>
```




## Les booléens


Nous avons  déjà pas mal  exploré les Booléens au  chapitre 1. En  `Python` leur
type est `bool`. Notons que c'est un  sous-type des entiers et que `True` est un
synonyme de `1` et `False` est un synonyme de `0`:

```python
>>> a = True
>>> a == 1
True
>>> 5*a
5
```

et avec un peu d'avance sur notre  programme, cela nous permettra de compter les
bonnes "réponses" à une "question":

```python
>>> def est_voyelle(c): return c in "aeiouy"

>>> texte = "Use the force young Padawan"

>>> sum(est_voyelle(c.lower()) for c in texte)
11
```

Il y a 11 voyelles dans la phrase *Use the force young Padawan*.


