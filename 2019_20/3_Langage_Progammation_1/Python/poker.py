#!/usr/bin/env python
# coding: utf-8


#####################
#
#  P O K E R
#
#####################
import random


couleurs = [chr(9824), chr(9827), chr(9829), chr(9830)]
hauteurs = [str(h) for h in range(1, 11)] + ['V', 'D', 'R']

jeu = [f"{h}{c}" for h in hauteurs for c in couleurs]

n = int(input("\nCombien de mains (moins de 10) voulez-vous afficher ? : "))

cartes_choisies = random.sample(jeu, 5*n)

for no_main in range(n):
        print(f"{' '.join([cartes_choisies[5*no_main + k] for k in range(5)])}\n")
