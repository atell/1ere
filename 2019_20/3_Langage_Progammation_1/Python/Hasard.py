#1 les imports
from random import randint
from typing import List

#2 fonctions
def hasard(n: int, a: int, b: int) -> List[int]:
    """
    Fonction qui renvoie une liste de n nombres entiers
    compris entre a et b
    """
    return [randint(a, b) for _ in range(n)]

#3 Programme principal
## Entrée
n = int(input("Combien de nombres : "))
a = int(input("Entre le nombre : "))
b = int(input("et le nombre : "))
## Traitement
L = hasard(n, a, b)
##Sortie
print(L)
