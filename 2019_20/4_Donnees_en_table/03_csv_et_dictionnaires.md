# CSV et dictionnaires - Enregistrements - Associations


## En Bref

Le format  CSV est couramment  utilisé pour échanger des  données habituellement
traitées  à   l'aide  de  tableurs   ou  de   logiciels  de  bases   de  données
principalement.  Nous allons  apprendre à  importer et  exporter des  données en
utilisant ce format.



## Enregistrements

Un enregistrement est une structure de données de types éventuellement différents
auxquelles on accède grâce à un nom.

Par  exemple,  on  peut  représenter  les  notes  d'un  élève  dans  différentes
discipliines à l'aide d'un enregistrement: 

```
{'Nom': 'Joe', 'Anglais': 17, 'Info': 18, 'Maths': 16}
```

Les *champs* (ou  *clés* ou *attributs*) de ces enregistrements  sont ici `Nom`,
`Anglais`, `Info`  et `Maths`. On leur  associe des *valeurs*, ici  `Joe`, `17`,
`18` et `16`.

En Python, on utilisera dans cet ouvrage les *dictionnaires* pour représenter les
enregistrements conformément au programme.


## Fichiers CSV 

Le format CSV (*Comma Separated Value*)  est couramment utilisé pour importer ou
exporter des  données d'une  feuille de  calcul d'un  tableur. C'est  un fichier
texte dans lequel  chaque ligne correspond à une ligne  du tableau, les colonnes
	étant séparées  par des virgules.  Il permet  donc de représenter  une liste
	d'enregistrements ayant les mêmes champs.


>Ce  format est  né bien  avant  les ordinateurs  personnels et  le format  `xls`
>puisque c'est en 1972 qu'il a été introduit.

Pour éviter les problèmes dus à l'absence de standardisation du séparateur
décimal (virgule en France, point ailleurs), mieux vaut paramétrer son
tableur pour utiliser le point (français suisse par exemple).
Voici un exemple de feuille de calcul:

![](IMG/tableur1.png){width=150}

Le fichier CSV correspondant étant:

```
'Nom','Anglais','Info','Maths'
'Joe','17','18','16'
'Zoé','15','17','19'
'Max','19','13','14'
```

## Lecture de fichiers CSV

On  peut   choisir  de  représenter  sur   python  les  fichiers  CSV   par  des
*listes* de *dictionnaires*  dont les *clés* seront les noms  des colonnes.

Pour reprendre l'exemple précédent, on obtient:

```python
Table1 = 
[{'Nom': 'Joe', 'Anglais': 17, 'Info': 18, 'Maths': 16},
 {'Nom': 'Zoé', 'Anglais': 15, 'Info': 17, 'Maths': 19},
 {'Nom': 'Max', 'Anglais': 19, 'Info': 13, 'Maths': 14}]
```



>En utilisant le vocabulaire habituel décrivant une feuille de calcul d'un tableur:
>* une table est une liste de dictionnaires: `Table1`;
>* une ligne est un dictionnaire: `Table1[0]`
>* une cellule est une valeur associée à une clé d'un dictionnaire: `Table1[0]['Info']`






Voici des propositions d'import/expot de fichiers CSV:

```python
import csv
def depuis_csv(nom_fichier_csv):
    """
    Crée une liste de dictionnaires, un par ligne.
    La 1ère ligne du fichier csv est considérée comme la ligne des noms des champs
    """
    lecteur = csv.DictReader(open(nom_fichier_csv,'r')) 
    return [dict(ligne) for ligne in lecteur]
```

## Export vers un fichier CSV

```python
def vers_csv(nom_table, nom_export, ordre_cols):
    """
    Exporte une liste de dictionnaires sous forme d'un
    fichier csv. On rentre le nom de la table sous forme de chaîne.
    On donne l'ordre des colonnes sous la forme d'une liste d'attributs.
    >>> vers_csv(Groupe1, 'newGroupe1', ordre_cols=['Nom','Anglais','Info','Maths'])
    """
    with open(nom_export + '.csv', 'w') as fic_csv:
        ecrit = csv.DictWriter(fic_csv, fieldnames=ordre_cols)
        ecrit.writeheader() # pour le 1ère ligne
        for ligne in nom_table:
            ecrit.writerow(ligne) # lignes ajoutées 1 à 1
    return None
```

## Pandas

Il existe une bibliothèque bien plus puissante et utilisée dans la vraie vie par
les informaticiens ayant à travailler sur des données : [`pandas`](https://pandas.pydata.org/pandas-docs/stable/)



## Sélection de lignes vérifiant un critère

On cherche à  créer une nouvelle table en extrayant  les lignes satisfaisant une
condition donnée sous la forme d'une fonction booléenne.



>Les opérateurs booléens  habituels sont `<`, `>`, `<=`, `>=`,  `==`, `!=`, `in`,
>`not`, `and`, `or`, `is`,...





Reprenons un tableau précédent:

```
      Nom  Maths  Anglais  Info
      Joe     16       17    18
      Zoé     19       15    17
      Max     14       19    13
```

On voudrait par exemple sélectionner les élèves ayant plus de 16 en Maths.
On n'oublie pas que toutes les valeurs sont sous forme de chaînes de caractères.
On  peut être  amené à  utiliser  la fonction  `eval` qui  permet d'évaluer  une
expression contenue dans une chaîne de caractères.

On rentre le critère de sélection sous la forme d'une chaîne de caractères:

```python
def select(table, critère):
    def test(ligne):
        return eval(critère)
    return [ligne for ligne in table if test(ligne)]

```

Par exemple:

```python
>>> select(Table1, "eval(ligne['Maths']) > 16")
```
Et on n'obtient bien que la ligne satisfaisant le critère:

```python
    [{'Info': '17', 'Anglais': '15', 'Maths': '19', 'Nom': 'Zoé'}]
```

## Projection

On  peut  également  vouloir  sélectionner   une  ou  plusieurs  colonnes  d'une
table.


Dans notre exemple, on peut vouloir ne retenir que les notes
d'info et le nom des élèves pour obtenir:


```
      Nom   Info
      Joe     18
      Zoé     17
      Max     13
```

On va donc créer une nouvelle table qui ne contiendra que ces attributs:


```python
def projection(table, liste_clés):
     return [{clé:ligne[clé] for clé in ligne if clé in liste_clés} for ligne in table]
```

Pour obtenir la table attendue, on entre:


```python
>>> projection(Table1, ['Nom', 'Info'])
```



## Tri d'une table selon une colonne


Puisqu'une table est représentée par une liste, on peut la trier en utilisant la
fonction de tri `sorted` qui dispose  d'un argument `key` permettant de préciser
selon quel critère (qui doit être une  fonction de variables les objets à trier)
une liste doit être triée.  Un troisième argument, `reverse` (un booléen),
permet de préciser si l'on veut le  résultat par ordre croissant (par défaut) ou
décroissant (en précisant `reverse=True`).


On peut alors généraliser pour n'importe  quelle table en donnant juste son nom,
l'attribut choisi  pour le  tri et  préciser si  l'on veut  obtenir le  tri dans
l'ordre décroissant. 


```python
def tri(table, attribut, decroit=False):
	def critère(ligne):
        return ligne[attribut]
    return sorted(table, key=critère, reverse=decroit)
```



Par exemple, si l'on veut trier dans l'ordre décroissant la table `Table1` introduite précédemment
selon les notes de Maths, on fera `tri(Table1, 'Maths', True)` qui donne:

```
   Nom Maths Info Anglais
0  Zoé    19   17      15
1  Joe    16   18      17
2  Max    14   13      19
```

## Fusion de tables

Supposons l'existence d'une seconde table `Table2` donnant l'âge et le courriel de
certains élèves:

```
   Nom Âge     Courriel
0  Joe  16  joe@info.fr
1  Zoé  15  zoe@info.fr
```


On voudrait regrouper les données des deux tables. Elles ont l'attribut `Nom` en
commun. On veut obtenir la table suivante:

```
   Nom Âge     Courriel Maths Info Anglais
0  Joe  16  joe@info.fr    16   18      17
1  Zoé  15  zoe@info.fr    19   17      15
```

On exclut la ligne concernant `Max` car il n'est pas présent dans la seconde table.

Pour chaque ligne de la première table, on ne va considérer dans la seconde
table que les lignes ayant la même valeur pour l'attribut choisi: ici le même nom.

On effectuera la jointure avec:

```python
jointure(Table1, Table2, 'Nom')
```


Cependant,  dans  certaines  tables,  l'attribut commun  peut  avoir  une  autre
appellation. Par exemple, le seconde table peut aussi exister sous la forme:


```
   Name Age        Email Maths   CS English
0  Joe   16  joe@info.fr    16   18      17
1  Zoé   15  zoe@info.fr    19   17      15
```

Cette fois, on précisera l'attribut de la seconde table:

```python
jointure(Table1, Table2, 'Nom', 'Name')
```

Voici une proposition de code:


```python
from copy import deepcopy

def jointure(table1, table2, cle1, cle2=None):
    if cle2 is None: # Par défaut les clés de jointure portent le même nom
        cle2 = cle1
    new_table = []  # La future table créée, vide au départ
    for ligne1 in table1: 
        for ligne2 in table2:
		    #  on ne  considère que  les lignes  où les  cellules de  l'attribut
		    # choisi sont identiques. 
            if ligne1[cle1] == ligne2[cle2]:
                new_line = deepcopy(ligne1) # on copie entièrement la ligne de table1 
                for cle in ligne2: # on copie la ligne de table2 sans répéter la
                                   # cellule de jointure
                    if cle != cle2:
                        new_line[cle] = ligne2[cle]
                new_table.append(new_line)
    return new_table
```



### From future import

En Terminale, vous découvrirez la gestion des bases de données relationelles, notamment à l'aide du langage SQL. Dans ce langage, la jointure donnée en exemple s'écrira:

```sql
SELECT Nom
FROM Table1 JOIN Table2
ON Table1.Nom = Table2.Nom
```


